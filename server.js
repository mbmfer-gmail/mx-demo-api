console.log(" >> Server Bootcamp Practitioner")

var express 		= require('express')
var bodyParser 		= require('body-parser')
var jsonQuery 		= require('json-query')
var requestJson		= require('request-json')
var stringify 		= require('json-stringify');

var movimientosjson = require('./movimientos.json')
var usuariosjson    = require('./usuarios.json')

var app = express()
app.use(bodyParser.json())

app.get('/', function(req, res){
	res.send('API:: Movimientos  /  Usuarios')
})

app.get('/v1/movimientos', function(req, res){
	res.send('mov_1,mov_2,mov_3')
})

app.get('/v2/movimientos', function(req, res){
		res.send( movimientosjson )
})
//  PARAMETROS OBLIGATORIOS
app.get('/v2/movimientosq/:id/:import', function(req, res){
		res.send( req.query )
})
//  PARAMETROS  OPCIONALES
app.get('/v2/movimientosp', function(req, res){
		res.send( req.params )
})
app.get('/v2/movimientos/:id', function(req, res){
	console.log( "id: ", req.params.id)
	res.send( movimientosjson[req.params.id-1 ] )
	console.log("--------------------ok")
})
app.post('/v2/movimientos',  function(req, res){
	var nuevo = req.body 
	console.log( "Data: ", nuevo) 
	nuevo.id  =  movimientosjson.length + 1
	console.log( "Nuevo ID: ", nuevo.id)
	movimientosjson.push(nuevo)
	res.send('Petición recibida: ' + nuevo.id )
})
app.put('/v2/movimientos',  function(req, res){
	var _cambios = req.body	
	var _id    = _cambios.id -1
	var _actual  = movimientosjson[ _id ]
	var _msj	= 'Cambios:  Actualizado   '
	console.log( "To DATA ", _cambios )
	console.log( "To ID ",   _id)
	if(_cambios.concept != undefined ){
		_actual.concept	= _cambios.concept
		_msj += ' concept: ' + _cambios.concept     
	}
	if(_cambios.date != undefined ){
		_actual.date	= _cambios.date
		_msj += ' date: ' + _cambios.date
	}	
	if(_cambios.detail != undefined ){
		_actual.detail	= _cambios.detail
		_msj += ' detail: ' + _cambios.detail
	}
	if(_cambios.import != undefined ){
		_actual.import	= _cambios.import
		_msj += ' import: ' + _cambios.import
	}
	movimientosjson[ _id  ] = _actual
	res.send( _msj )
})
app.delete('/v2/movimientos',  function(req, res){
	var _cambios = req.body	
	var _id      = _cambios.id -1
	var _actual  = movimientosjson[ _id ]
	var _msj	 = 'Cambios:  Baja   '
	console.log( " ID: ", - _actual.id)

	_actual.import	= _cambios.import * -1
	_actual.concept	= "Se da de baja con vaor NEGATIVO "
	_msj += ' Import: '  +  _actual.import 
		 +  ' Concept: ' +  _actual.concept

	movimientosjson[ _id  ] = _actual
	res.send( _msj )
	
})


//  PRACTICA
//	Agregar  api  Usuarios
//      Datos:     https://www.mockaroo.com/
//		Usuarios:  id, email, pass, nombre, estado [online, offline]
//		GET  id	 
//		POST logIn  / email / pwd
//		POST logOut / id

app.get('/v2/usuarios', function(req, res){
		res.send( usuariosjson )
})
app.get('/v2/usuarios/:id', function(req, res){
	console.log( "id: ", req.params.id)
	res.send( usuariosjson[req.params.id-1 ] )
	console.log("--------------------ok")
})
app.post('/v2/usuarios/login', function(req, res){
	var email 		= req.body.email
	var password 	= req.body.password 
	console.log( "Data: Email: ", email + " Password: " + password)

	var _result = jsonQuery('[email=' + email + ']',{data:usuariosjson})
	if(_result.value!=null && _result.value.password == password ){
		usuariosjson[ _result.value.id-1].estado='logged'
		res.send( '{"login":"OK"}')
	}
	else{
		res.send( '{"login":"Error"}')
	}
})

app.post('/v2/usuarios/logout', function(req, res){
	var email 		= req.body.email
	console.log( "logout - Data: Email: ", email)
	var _result = jsonQuery('[email=' + email + ']',{data:usuariosjson})
	if(_result.value!=null ){
		usuariosjson[ _result.value.id-1].estado='offline'
		res.send( '{"logout":"OK", "estado":"offline"}')
	}
	else{
		res.send( '{"logout":"Error", "estado":"offline"}')
	}
})


//	PRACTICA # 3
//	API CONECTADA A MLAB
var  mlab_base_path 	= "https://api.mlab.com/api/1/databases/mx-demo-techui-fm/collections"
var  mlab_app_key   	= "apiKey=7DiYqBGNHsLCbWveVW9qAjQ0ATuXQ2VE"
var  mlab_client		=  requestJson.createClient( mlab_base_path + "?" + mlab_app_key )

//https://api.mlab.com/api/1/databases/mx-demo-techui-fm/collections?apiKey=7DiYqBGNHsLCbWveVW9qAjQ0ATuXQ2VE
//	[ "movimientos" , "movs" , "system.indexes" , "usuarios" ]


app.get('/v3/', function(req, res){
	
	mlab_client.get( '' , function(_error, _resM, _body ){
		if(!_error){
			var user_collections = []
			for (var i = _body.length - 1; i >= 0; i--) {
				if(_body[i]== "system.indexes" ) { continue; }
				if(_body[i]== "movs") { continue; }
				else{
					user_collections.push ( {"recurso":_body[i],"url":"/v3/"+_body[i] })
				}
			}
			res.send( user_collections )
		}	
		else{
			res.send( _error )
		}
	})
		
	
})
// ____________________________________________USUARIOS
app.get('/v2/usuarios', function(req, res){
		res.send( usuariosjson )
})

app.post('/v3/usuarios', function(req, res){
	mlab_client		=  requestJson.createClient( mlab_base_path + "/usuarios?" + mlab_app_key )
	mlab_client.post( '' , req.body , function(error, _resM , _body){
		res.send( _body )
	})
})

app.post('/v3/usuarios/:id', function(req, res){	
	mlab_client		=  requestJson.createClient( mlab_base_path + "/usuarios" )
	var query = '?q={"id":'+req.params.id+'}&'
	console.log (query)
	mlab_client.get( query + mlab_app_key, function(error, _resM , _body){
		res.send( _body )
	})
})

app.put('/v3/usuarios/:id', function(req, res){	
	mlab_client		= requestJson.createClient( mlab_base_path + "/usuarios" )
	var query  		= '?q={"id":'+req.params.id+'}&'
	var cambios 	= '&{ "$set":{'+stringify(req.body)+'}}'
	console.log ("cambios: Y	"  +cambios)
	mlab_client.put( query + mlab_app_key, function(error, _resM , cambios){
		res.send( cambios )
	})
})
// ____________________________________________MOVIMIENTOS

app.get('/v3/movimientos', function(req, res){
	mlab_client		=  requestJson.createClient( mlab_base_path + "/movimientos?" + mlab_app_key )
	mlab_client.get( '' , function(_error, _resM, _body ){
		if(!_error){		
			res.send( _body )
		}	
		else{
			res.send( _error )
		}
	})
})

app.post('/v3/movimientos', function(req, res){
	mlab_client		=  requestJson.createClient( mlab_base_path + "/movimientos?" + mlab_app_key )
	mlab_client.post( '' , req.body , function(error, _resM , _body){
		res.send( _body )
	})
})

app.post('/v3/movimientos/:id', function(req, res){	
	mlab_client		=  requestJson.createClient( mlab_base_path + "/movimientos" )
	var query = '?q={"idmovimiento":'+req.params.id+'}&'
	console.log (query)
	mlab_client.get( query + mlab_app_key, function(error, _resM , _body){
		res.send( _body )
	})
})

app.put('/v3/movimientos/:id', function(req, res){	
	mlab_client		= requestJson.createClient( mlab_base_path + "/movimientos" )
	var query  		= '?q={"idmovimiento":'+req.params.id+'}&'
	var cambios 	= '&{ "$set":{'+stringify(req.body)+'}}'
	console.log ("cambios: Y	"  +cambios)
	mlab_client.put( query + mlab_app_key, function(error, _resM , cambios){
		res.send( cambios )
	})
})





//	PRACTICA # 4
//	API DESCARGAR UN PDR  GET


app.get('/v4', function(req, res){
	console.log ('descargar ')	
	res.sendfile('./mb65379.pdf')	
});


app.listen(3000)
console.log(" >> Server active port [ 3000 ] ")

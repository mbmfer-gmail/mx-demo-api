var  mocha 		= require('mocha')
var  chai 		= require('chai')		// motor de reglas
var  chaiHttp 	= require('chai-http')
var  should		= chai.should()
var  server     = require('../server')
chai.use(chaiHttp)  // configura  chai con el modulo http

describe('Test de conectividad', () => {
	it('Google funciona',(done) => {
		chai.request('http://www.google.com.mx').get('/').end((err, res) =>{
			//console.log(res)
			res.should.have.status(200)
			done()
		})
	})
})

describe('Test de API de Usuarios', () => {

	it('Exist API',(done) => {
		chai.request('http://localhost:3000').get('/v3')
			.end((err, res) =>{
			//console.log(res)
			res.should.have.status(200)
			done()
		})
	})
	it('Test de API / Raiz',(done) => {
		chai.request('http://localhost:3000').get('/v3')
			.end((err, res) =>{
				//console.log(res)
			res.should.have.status(200)
			res.body.should.be.a('array')
			done()
		})
	})
	it('Test de API devuelve 2 colecciones',(done) => {
		chai.request('http://localhost:3000').get('/v3')
			.end((err, res) =>{
			//console.log(res.body)
			res.should.have.status(200)
			res.body.should.be.a('array')
			res.body.length.should.be.eql(2)
			done()
		})
	})	
	it('Test de API devuelve objetos correctos ',(done) => {
		chai.request('http://localhost:3000').get('/v3')
			.end((err, res) =>{
			//console.log(res.body)
			res.should.have.status(200)
			res.body.should.be.a('array')
			res.body.length.should.be.eql(2)
			for (var i = 0; i < res.body.length; i++) {
				res.body[i].should.have.property('recurso')
				res.body[i].should.have.property('url')
			}
			done()
		})
	})
})

describe('Test de API / Movimientos ', () => {

	it('Exist API ',(done) => {
		chai.request('http://localhost:3000').get('/v3/movimientos')
			.end((err, res) =>{
				//console.log(res)
			res.should.have.status(200)
			done()
		})
	})
	it('API / Movimientos',(done) => {
		chai.request('http://localhost:3000').get('/v3/movimientos')
			.end((err, res) =>{
				//console.log(res)
			res.should.have.status(200)
			res.body.should.be.a('array')
			done()
		})
	})
	/*
	it('API / Movimientos por ID',(done) => {
		chai.request('http://localhost:3000').get('/v3/movimientos/2')
			.end((err, res) =>{
				console.log(res)
			res.should.have.status(200)
			res.body.should.be.a('array')
			done()
		})
	})
	*/
})
